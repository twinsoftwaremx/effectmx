﻿using EffectMX.Controls;
using EffectMX.Models;
using EffectMX.Resources;
using ImageProcessingApp.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Media.PhoneExtensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace EffectMX
{
    public partial class StreamPage : PhoneApplicationPage
    {
		public StreamPage()
		{
			InitializeComponent();
		}

		private void PickPhotoButtonClick(object sender, RoutedEventArgs e)
		{
			var photoChooserTask = new PhotoChooserTask();
			photoChooserTask.Completed += photoChooserTaskCompleted;
			photoChooserTask.Show();
		}

		void photoChooserTaskCompleted(object sender, PhotoResult e)
		{
			if (e.ChosenPhoto == null)
			{
				return;
			}

			using (MemoryStream stream = new MemoryStream())
			{
				e.ChosenPhoto.CopyTo(stream);

				App.PhotoModel = new PhotoModel() { Buffer = stream.GetWindowsRuntimeBuffer() };
				App.PhotoModel.Captured = false;
				App.PhotoModel.Dirty = false;
				App.PhotoModel.Path = e.OriginalFileName;
			}

			NavigationService.Navigate(new Uri("/Pages/PhotoPage.xaml", UriKind.Relative));
		}

		private void AboutButton_Click(object sender, RoutedEventArgs e)
		{
			NavigationService.Navigate(new Uri("/Pages/AboutPage.xaml", UriKind.Relative));
		}

		private void TakePicButton_Click(object sender, RoutedEventArgs e)
		{
			var cameraTask = new CameraCaptureTask();
			cameraTask.Completed += cameraTask_Completed;
			cameraTask.Show();
		}

		void cameraTask_Completed(object sender, PhotoResult e)
		{
			if (e.ChosenPhoto == null)
			{
				return;
			}

			using (MemoryStream stream = new MemoryStream())
			{
				e.ChosenPhoto.CopyTo(stream);

				App.PhotoModel = new PhotoModel() { Buffer = stream.GetWindowsRuntimeBuffer() };
				App.PhotoModel.Captured = false;
				App.PhotoModel.Dirty = false;
				App.PhotoModel.Path = e.OriginalFileName;
			}

			NavigationService.Navigate(new Uri("/Pages/PhotoPage.xaml", UriKind.Relative));
		}

    }
}