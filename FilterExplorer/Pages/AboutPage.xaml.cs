﻿using EffectMX.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System;
using System.Xml.Linq;

namespace ImageProcessingApp.Pages
{
    public partial class AboutPage : PhoneApplicationPage
    {
        public AboutPage()
        {
            InitializeComponent();

            VersionTextBlock.Text = String.Format(AppResources.AboutPage_TextBlock_Version,
                XDocument.Load("WMAppManifest.xml").Root.Element("App").Attribute("Version").Value);
        }

		protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
		{
			base.OnNavigatedTo(e);

			string[] filters = new string[] {
				"Antique", "Brightness", "Color Adjust", "Contrast", "Flip", "Grayscale",
				"Hue Saturation", "Lomo", "Milky", "Mirror", "Monocolor", "Moonlight",
				"Negative", "Paint", "Posterize", "Sepia", "Sharpness", "Sketch", "Rotate", "Warp Twister", "Watercolor", "Vignetting",
				"Auto Enhance", "Auto Levels", "Color Boost", "Exposure", "Foundation", "Levels", "Local Boost",
				"Temperature & Tint", "Whiteboard", "Lens Blur"
			};
			FiltersListBox.ItemsSource = filters;
		}

		private void ContactButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			var emailTask = new EmailComposeTask();
			emailTask.Subject = "EffectMX WP8";
			emailTask.To = "twinsoftware@hotmail.com";
			emailTask.Show();
		}

		private void RateButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			var marketplaceTask = new MarketplaceReviewTask();
			marketplaceTask.Show();
		}
    }
}